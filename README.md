**FORBIDDEN LOVE**

Our first group project developed at <Academia_de_Código_>


**// The idea**

We were challenged to develop a game in one week, in groups of 5, to develop both our software development and teamwork skills.
We decided to develop a game based on 80's games with a modern-day twist, addressing issues such as homosexuality. 
In our game, we have a couple that love each other but is separated by some enemies, so one of the persons has to face them in order to meet the other one.


**// The Development**

In the beginning, we were a little scared since this was our first big project but soon realized that we were capable of face this challenge as a team. 
The game was developed remotely since we were in a pandemic context, so it demanded us to improve not only our communication skills, but also our ability to work with version control systems (GitLab, in this case).


**// My role**

My role was working on the player movement, his collision with the final enemy and the sprites of the couple, as well as of the final enemy. 
Despite, we were always working together and helping each other solving the problems we were facing, so every team-member contributed on every part of our game.


**// Tech && Methodologies**

Java, IntelliJ, Simple Gfx
