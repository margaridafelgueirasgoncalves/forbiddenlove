package org.academiadecodigo.team2.forbiddenlove;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent.*;

public class GameKeyboard {

    private KeyboardHandler keyboardHandler;

    public GameKeyboard(KeyboardHandler keyboardHandler) {
        this.keyboardHandler = keyboardHandler;
    }

    public void setup() {
        Keyboard keyboard = new Keyboard(keyboardHandler);

        KeyboardEvent spaceBar = new KeyboardEvent();
        spaceBar.setKey(KEY_SPACE);
        spaceBar.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(spaceBar);

        KeyboardEvent spaceBarR = new KeyboardEvent();
        spaceBarR.setKey(KEY_SPACE);
        spaceBarR.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        keyboard.addEventListener(spaceBarR);

        KeyboardEvent rightArrow = new KeyboardEvent();
        rightArrow.setKey(KEY_RIGHT);
        rightArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rightArrow);

        KeyboardEvent leftArrow = new KeyboardEvent();
        leftArrow.setKey(KEY_LEFT);
        leftArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(leftArrow);

        KeyboardEvent upArrow = new KeyboardEvent();
        upArrow.setKey(KEY_UP);
        upArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(upArrow);

        KeyboardEvent downArrow = new KeyboardEvent();
        downArrow.setKey(KEY_DOWN);
        downArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(downArrow);

        KeyboardEvent letterS = new KeyboardEvent();
        letterS.setKey(KEY_S);
        letterS.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(letterS);

        KeyboardEvent letterM = new KeyboardEvent();
        letterM.setKey(KEY_M);
        letterM.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(letterM);

        KeyboardEvent letterE = new KeyboardEvent();
        letterE.setKey(KEY_E);
        letterE.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(letterE);

    }

}
