package org.academiadecodigo.team2.forbiddenlove.screen;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Screen {

    //Propriedades

    public static final int WIDTH = 525;
    public static final int HEIGHT = 675;
    public static final int PADDING = 10;
    private Rectangle field;
    private Picture picture;



    //Construtor
    public Screen(){
        field = new Rectangle(PADDING, PADDING, WIDTH, HEIGHT);
        field.setColor(Color.YELLOW);
        field.draw();

    }

    //Métodos
    public void setPicture(Picture picture) {
        this.picture = picture;
    }
}
