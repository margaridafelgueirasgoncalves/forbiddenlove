package org.academiadecodigo.team2.forbiddenlove.screen;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.gameobject.Princess;
import org.academiadecodigo.team2.forbiddenlove.gameobject.enemies.*;
import org.academiadecodigo.team2.forbiddenlove.gameobject.platforms.Bricks;
import org.academiadecodigo.team2.forbiddenlove.gameobject.platforms.Ladders;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;

public class Lvl2 extends Screen {

    private Picture fundo;
    private Picture guideText;
    private Enemy[] enemies;
    private Monster fabio;
    private Princess paulo;

    public void create() {

        fundo = new Picture(15,15,"resources/images/background522x670.png");
        fundo.draw();
        guideText = new Picture (100,615,"resources/images/guidetext.png");
        guideText.draw();

        Bricks ceiling = new Bricks(15, 0, "resources/images/tile-510x15.png");
        Bricks leftWall = new Bricks(0, 0, "resources/images/tile-15x689.png");
        Bricks rightWall = new Bricks(525, 0, "resources/images/tile-15x689.png");
        Bricks floor = new Bricks(15, 675, "resources/images/tile-510x15.png");

        Bricks sixFloor = new Bricks(15, 150,"resources/images/tile-465x15.png");
        Bricks sixFloorBegin = new Bricks(510, 150, "resources/images/tile-15.png");
        Bricks fiveFloor = new Bricks(60, 255,"resources/images/tile-465x15.png");
        Bricks fiveFloorEnd = new Bricks (15,255, "resources/images/tile-15.png");
        Bricks fourFloor = new Bricks(15, 360,"resources/images/tile-465x15.png");
        Bricks fourFloorBegin = new Bricks(510,360, "resources/images/tile-15.png");
        Bricks threeFloor = new Bricks(60, 465,"resources/images/tile-465x15.png");
        Bricks threeFloorEnd = new Bricks (15, 465, "resources/images/tile-15.png");
        Bricks twoFloor = new Bricks(15, 570,"resources/images/tile-465x15.png");
        Bricks twoFloorBegin = new Bricks(510,570, "resources/images/tile-15.png");

        Ladders ladderFirstF = new Ladders(480,570);
        Ladders ladderSecondF = new Ladders(30,465);
        Ladders ladderThirdF = new Ladders(480,360);
        Ladders ladderFourthF = new Ladders(30,255);
        Ladders ladderFifthF = new Ladders(480,150);

        enemies = new Enemy[] {
                new Barras(this.makeNewPosition(110,175),Direction.RIGHT,"resources/images/dildogrande.png"),
                new Barras(this.makeNewPosition(350,175),Direction.RIGHT,"resources/images/dildogrande2.png"),
                new Barras(this.makeNewPosition(270,175),Direction.RIGHT,"resources/images/dildogrande1.png"),
                new Barras(this.makeNewPosition(190,175),Direction.RIGHT, "resources/images/dildogrande3.png"),
                new Barras(this.makeNewPosition(430,175),Direction.RIGHT,"resources/images/dildogrande4.png"),
                new Balas(this.makeNewPosition(280, 290), Direction.RIGHT),
                new Balas(this.makeNewPosition(380, 350), Direction.RIGHT),
                new Balas(this.makeNewPosition(180, 350), Direction.RIGHT),
                new Balas(this.makeNewPosition(480, 310), Direction.RIGHT),
                new Balas(this.makeNewPosition(80, 290), Direction.RIGHT),
                new CimaBaixo(this.makeNewPosition(120, 430), Direction.DOWN),
                new CimaBaixo(this.makeNewPosition(220, 440), Direction.UP),
                new CimaBaixo(this.makeNewPosition(320, 390), Direction.DOWN),
                new CimaBaixo(this.makeNewPosition(420, 400), Direction.UP),
                new Caganitas(this.makeNewPosition(80, 550), Direction.LEFT),
                new Caganitas(this.makeNewPosition(230, 550), Direction.LEFT),
                new Caganitas(this.makeNewPosition(400, 550), Direction.LEFT)
        };

        paulo = new Princess();
        fabio = new Monster(this.makeNewPosition(25, 115), Direction.RIGHT);

    }

    public Enemy[] getEnemies() { return enemies;}

    public Position makeNewPosition(int x, int y) {
        return new Position(x,y,this);
    }

    public Monster getMonster() { return fabio; }

    public void lvl2Disappear() {
        fabio.fabioDisappear();
        paulo.pauloDisappear();
        for (int i=0; i<enemies.length; i++) {
            enemies[i].disappear();
        }
    }

}
