package org.academiadecodigo.team2.forbiddenlove.screen;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Win extends Screen {

    Picture picture;

    public Win() {
        this.picture = new Picture(10, 10, "resources/images/imagemFinal.png");
        picture.draw();
    }

    public void show() {
        picture.draw();
    }

    public void disappear () {
        picture.delete();
    }

}
