package org.academiadecodigo.team2.forbiddenlove.screen;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameOver extends Screen{

    Picture picture;

    public GameOver() {
        this.picture = new Picture(10, 10, "resources/images/gameOverImage.png");
    }

    public void show() {
        picture.draw();
    }

    public void disappear() {picture.delete(); }
}
