package org.academiadecodigo.team2.forbiddenlove.screen;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class MainMenu extends Screen{

    Rectangle rectangle;
    Rectangle rectangle2;
    Picture pressSToStart;
    Picture forbidden;
    Picture love;
    Picture paulo;
    Picture luis;
    Picture fabio;

    // Blinking Press S
    boolean isShown = true;

    public MainMenu() {
        rectangle = new Rectangle (15,15,515,665);
        rectangle2 = new Rectangle (10,10,525,675);
        rectangle2.setColor(Color.GREEN);
        rectangle2.fill();
        rectangle.setColor(Color.YELLOW);
        rectangle.fill();
        pressSToStart = new Picture(75,490,"resources/images/PRESSSTOSTART.png");
        forbidden = new Picture(16,70, "resources/images/Forbidden.png");
        love = new Picture(40,280,"resources/images/Love.png");
        paulo = new Picture(340,560,"resources/images/pauloface.png");
        luis = new Picture(140,560,"resources/images/luisface.png");
        fabio = new Picture(240,560,"resources/images/fabioface.png");
    }

    public void show() throws InterruptedException {
        forbidden.draw();
        love.draw();
        paulo.draw();
        luis.draw();
        fabio.draw();
    }

    public void blink() {
        if (isShown) {
            pressSToStart.delete();
            isShown = false;
            return;
        }
        pressSToStart.draw();
        isShown = true;
    }

    public void disappear() {
        pressSToStart.delete();
        forbidden.delete();
        love.delete();
        rectangle.delete();
        rectangle2.delete();
        paulo.delete();
        luis.delete();
        fabio.delete();
    }
}
