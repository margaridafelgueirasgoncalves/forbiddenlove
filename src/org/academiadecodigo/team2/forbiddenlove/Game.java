package org.academiadecodigo.team2.forbiddenlove;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.team2.forbiddenlove.gameobject.Player;
import org.academiadecodigo.team2.forbiddenlove.gameobject.enemies.Enemy;
import org.academiadecodigo.team2.forbiddenlove.gameobject.enemies.Monster;
import org.academiadecodigo.team2.forbiddenlove.movement.CollisionDetector;
import org.academiadecodigo.team2.forbiddenlove.movement.CollisionDetectorMonster;
import org.academiadecodigo.team2.forbiddenlove.screen.GameOver;
import org.academiadecodigo.team2.forbiddenlove.screen.Lvl2;
import org.academiadecodigo.team2.forbiddenlove.screen.MainMenu;
import org.academiadecodigo.team2.forbiddenlove.screen.Win;

import java.util.concurrent.TimeUnit;

import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent.*;

public class Game implements KeyboardHandler {

    private Player player;
    private MainMenu menu;
    private Lvl2 lvl2;
    private GameOver gameOver;
    private Music musicBox;
    private GameKeyboard gamekeyboard;
    private Enemy[] enemies;
    private Monster fabio;
    private CollisionDetector collisionDetector;
    private CollisionDetectorMonster collisionDetectorMonster;
    private boolean start = false;
    private boolean over = false;
    private Win win;


    public void init() throws InterruptedException {

        gamekeyboard = new GameKeyboard(this);
        gamekeyboard.setup();
        menu = new MainMenu();
        menu.show();
        musicBox = new Music();
        musicBox.introMusicPlay();

        while (!start) {
            menu.blink();
            TimeUnit.MILLISECONDS.sleep(500);
        }
        start();
    }

    private void start() throws InterruptedException {

        menu.disappear();
        if(over) {
            gameOver.disappear();
            over = false;
        }

        if(!musicBox.isMute()) {
            musicBox.startMusicMain();
        }
        musicBox.setIntro(false);
        musicBox.setWin(false);
        musicBox.setGameOver(false);
        musicBox.setMain(true);

        TimeUnit.MILLISECONDS.sleep(500);

        lvl2 = new Lvl2();
        lvl2.create();
        player = new Player(lvl2.makeNewPosition(25, 640), musicBox);
        player.create();
        player.livesStarter();
        enemies = lvl2.getEnemies();
        fabio = lvl2.getMonster();

        collisionDetector = new CollisionDetector(player, enemies);
        collisionDetectorMonster = new CollisionDetectorMonster(player, fabio);

        while (!player.isPlayerDead()) {
            TimeUnit.MILLISECONDS.sleep(45);
            for (Enemy enemy : enemies) {
                enemy.move();
            }
            collisionDetector.check();

            fabio.move();
            collisionDetectorMonster.check();

            if(player.getWinner()) {
                theEnd();
                break;
            }
        }

        TimeUnit.MILLISECONDS.sleep(500);
        gameOver();

    }

    public void gameOver() throws InterruptedException {

        start = false;

        gameOver = new GameOver();
        gameOver.show();

        if(!musicBox.isMute()) {
            musicBox.playGameOverMusic();
        }
        musicBox.setMain(false);
        musicBox.setGameOver(true);

        while (!start) {
            TimeUnit.MILLISECONDS.sleep(500);
        }
        start();
    }

    public void theEnd() throws InterruptedException {

        start = false;
        lvl2.lvl2Disappear();
        fabio.fabioDisappear();
        player.playerDisappear();

        if(!musicBox.isMute()) {
            musicBox.playWin();
        }
        musicBox.setMain(false);
        musicBox.setWin(true);

        win = new Win();

        while (!start) {
            TimeUnit.MILLISECONDS.sleep(500);
        }
        win.disappear();
        start();
    }

    public boolean getStart() { return start;}

    //Teclado

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch(keyboardEvent.getKey()) {
            case KEY_SPACE:
                player.jump();
                break;
            case KEY_RIGHT:
                player.moveRight();
                break;
            case KEY_LEFT:
                player.moveLeft();
                break;
            case KEY_UP:
                player.moveUp();
                break;
            case KEY_DOWN:
                player.moveDown();
                break;
            case KEY_S:
                start = true;
                break;
            case KEY_M:
                musicBox.mute();
                break;
            case KEY_E:
                System.exit(0);
                break;
        }
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

        if(keyboardEvent.getKey() == KEY_SPACE) {
            player.afterJump();
        }
    }

}
