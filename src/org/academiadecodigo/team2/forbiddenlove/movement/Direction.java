package org.academiadecodigo.team2.forbiddenlove.movement;

public enum Direction {

    UP,
    DOWN,
    LEFT,
    RIGHT;

}
