package org.academiadecodigo.team2.forbiddenlove.movement;

import org.academiadecodigo.team2.forbiddenlove.gameobject.Player;
import org.academiadecodigo.team2.forbiddenlove.gameobject.enemies.Monster;

public class CollisionDetectorMonster {

    private Monster fabio;
    private Player player;

    public CollisionDetectorMonster (Player player, Monster fabio) {
        this.fabio = fabio;
        this.player = player;
    }

    public void check () throws InterruptedException {
            if (luisTouchesFabio(fabio, player)) {
                player.setWinner(true);
            } else if (fabioTouchesLuis(fabio,player)) {
                player.die();
            }
    }


    public boolean fabioTouchesLuis(Monster fabio, Player player) {
        int fabioMinX = fabio.getPos().getX();
        int fabioMaxX = fabio.getPos().getX() + fabio.getWidth();
        int playerMinX = player.getPos().getX();
        int playerMaxX = player.getPos().getX() + player.getWidth();

        int fabioMinY = fabio.getPos().getY();
        int fabioMaxY = fabio.getPos().getY() + fabio.getHeight();
        int playerMinY = player.getPos().getY();
        int playerMaxY = player.getPos().getY() + player.getHeight();

        return checkTouchXOrY(fabioMinX, fabioMaxX, playerMinX, playerMaxX) &&
                checkTouchXOrY(fabioMinY, fabioMaxY, playerMinY, playerMaxY);
    }


    public boolean luisTouchesFabio(Monster fabio, Player player) {
        int fabioMinX = fabio.getPos().getX();
        int fabioMaxX = fabio.getPos().getX() + fabio.getWidth();
        int playerMinX = player.getPos().getX();
        int playerMaxX = player.getPos().getX() + player.getWidth();

        int fabioMinY = fabio.getPos().getY();
        int playerMaxY = player.getPos().getY() + player.getHeight();

        return checkTouchXOrY(fabioMinX, fabioMaxX, playerMinX, playerMaxX) &&
                killFabio(fabioMinY, playerMaxY);
    }


    public boolean checkTouchXOrY(int min0, int max0, int min1, int max1){
        return Math.max(min0,max0) >= Math.min(min1,max1) &&
                Math.min(min0,max0) <= Math.max(min1,max1);
    }

    public boolean killFabio(int min0, int max1){
        return min0 == max1;
    }
}
