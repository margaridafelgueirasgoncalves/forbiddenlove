package org.academiadecodigo.team2.forbiddenlove.movement;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.team2.forbiddenlove.screen.Screen;

public class Position {

    private int x;
    private int y;
    private Screen grid;
    Rectangle rectangle;

    public Position(int x, int y, Screen grid) {
        this.grid = grid;
        this.x = x;
        this.y = y;
    }

    public Screen getGrid() { return grid; }

    public void setX(int x) { this.x = x;}

    public void setY(int y) {this.y = y;}

    public int getY() { return y; }

    public int getX() { return x; }

    public void moveDown() {
        if (y + Screen.PADDING/2 <= Screen.HEIGHT) {
            y += Screen.PADDING/4;
        }
    }

    public void moveUp() {
        if (y - Screen.PADDING/2 >= 10) {
            y -= Screen.PADDING/4;
        }
    }

    public void moveLeft() {
        if (x - Screen.PADDING/2 >= 10) {
            x -= Screen.PADDING/2;
        }
    }

    public void moveRight() {
        if (x + Screen.PADDING/2 <= Screen.WIDTH) {
            x += Screen.PADDING/2;
        }
    }

    public void moveInDirection(Direction direction) {

        switch (direction) {

            case UP:
                moveUp();
                break;
            case DOWN:
                moveDown();
                break;
            case LEFT:
                moveLeft();
                break;
            case RIGHT:
                moveRight();
                break;
        }
    }
}
