package org.academiadecodigo.team2.forbiddenlove.movement;

import org.academiadecodigo.team2.forbiddenlove.Music;
import org.academiadecodigo.team2.forbiddenlove.gameobject.Player;
import org.academiadecodigo.team2.forbiddenlove.gameobject.enemies.Enemy;

public class CollisionDetector {

    private Enemy[] enemies;
    private Player player;

    public CollisionDetector (Player player, Enemy[] enemies) {
        this.enemies = enemies;
        this.player = player;
    }

    public void check () throws InterruptedException {
        for (Enemy enemy : enemies) {
            if (touch(enemy,player)) {
                player.die();
            }
        }
    }

    public boolean touch(Enemy enemy, Player player) {
        int enemyMinX = enemy.getPos().getX();
        int enemyMaxX = enemy.getPos().getX() + enemy.getWidth();
        int playerMinX = player.getPos().getX();
        int playerMaxX = player.getPos().getX() + player.getWidth();

        int enemyMinY = enemy.getPos().getY();
        int enemyMaxY = enemy.getPos().getY() + enemy.getHeight();
        int playerMinY = player.getPos().getY();
        int playerMaxY = player.getPos().getY() + player.getHeight();

        return checkTouchXOrY(enemyMinX, enemyMaxX, playerMinX, playerMaxX) &&
                checkTouchXOrY(enemyMinY, enemyMaxY, playerMinY, playerMaxY);
    }

    public boolean checkTouchXOrY(int min0, int max0, int min1, int max1){
        return Math.max(min0,max0) >= Math.min(min1,max1) &&
                Math.min(min0,max0) <= Math.max(min1,max1);
    }
}
