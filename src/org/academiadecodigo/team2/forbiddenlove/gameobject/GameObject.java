package org.academiadecodigo.team2.forbiddenlove.gameobject;

public class GameObject {

    protected int x;
    protected int y;
    protected GameObjectType type;

    public GameObject (int x, int y, GameObjectType type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

}
