package org.academiadecodigo.team2.forbiddenlove.gameobject;

import org.academiadecodigo.bootcamp.Sound;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.Music;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;
import org.academiadecodigo.team2.forbiddenlove.screen.Screen;

import java.util.concurrent.TimeUnit;

public class Player extends GameObject{

    private final int height = 45;
    private final int width = 30;
    private final int moveSize = 15;
    private Picture rectangle;
    private Position pos;
    private int lives;
    private boolean playerDead = false;
    private Picture life1;
    private Picture life2;
    private Picture life3;
    private final int brickWidth = 15;
    private final int jumpSize = 45;
    private int posXInit=25;
    private int posYInit=640;
    private int jumpCounter;
    private final int maxJumpMoves = 5;
    private boolean winner;
    private Music musicBox;


    public Player(Position pos, Music musicBox) {
        super(pos.getX(), pos.getY(), GameObjectType.PLAYER);
        this.pos = pos;
        this.musicBox = musicBox;
        x = 25;
        y = 640;
        pos.setX(x);
        pos.setY(y);
        rectangle = new Picture(x, y, "resources/images/Luis_gameSize_quadradinho.png");
        lives = 3;
        jumpCounter = 0;
        winner = false;
    }


    public void setWinner(boolean win) { winner = win; }

    public boolean getWinner() {return winner;}

    public void getCenas() {
        System.out.println("x: " + rectangle.getX() + "y: " + rectangle.getY());
    }

    public void create() {
        rectangle.draw();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getMoveSize() {
        return moveSize;
    }

    public Position getPos() { return pos;}

    public int getLives() {
        return lives;
    }

    public void moveRight() {
        if (jumpCounter < maxJumpMoves && jumpCounter > 0) {
            jumpCounter ++;
        }
        if (jumpCounter >= maxJumpMoves) {
            afterJump();
            return;
        }
        if (rectangle.getX() + getWidth() + getMoveSize() < Screen.WIDTH + Screen.PADDING) {
            rectangle.translate(getMoveSize(), 0);
            pos.setX(rectangle.getX());
            pos.setY(rectangle.getY());

        } else {
            if (jumpCounter < maxJumpMoves && jumpCounter > 0) {
                jumpCounter ++;
            }
            if (jumpCounter >= maxJumpMoves) {
                afterJump();
                return;
            }
            if (rectangle.getX() + getWidth() < Screen.WIDTH + Screen.PADDING) {
                rectangle.translate((Screen.WIDTH + Screen.PADDING) - rectangle.getX() - getWidth(), 0);
                pos.setX(rectangle.getX());
                pos.setY(rectangle.getY());
            }
        }
    }

    public void moveLeft() {
        if (jumpCounter < maxJumpMoves && jumpCounter > 0) {
            jumpCounter ++;
        }
        if (jumpCounter >= maxJumpMoves) {
            afterJump();
            return;
        }
        if (rectangle.getX() > Screen.PADDING + rectangle.getWidth() + brickWidth) {
            rectangle.translate(-getMoveSize(), 0);
            pos.setX(rectangle.getX());
            pos.setY(rectangle.getY());
        } else {
            if (jumpCounter < maxJumpMoves && jumpCounter > 0) {
                jumpCounter ++;
            }
            if (jumpCounter >= maxJumpMoves) {
                afterJump();
                return;
            }
            if (rectangle.getX() > Screen.PADDING + brickWidth) {
                rectangle.translate(-(rectangle.getX() - Screen.PADDING - brickWidth), 0);
                pos.setX(rectangle.getX());
                pos.setY(rectangle.getY());
            }
        }
    }

    public void moveDown() {
        if (((rectangle.getX() == 490) && (rectangle.getY() == 535)) || ((rectangle.getX() == 40) && (rectangle.getY() == 430)) || ((rectangle.getX() == 490) && (rectangle.getY() == 325)) || ((rectangle.getX() == 40) && (rectangle.getY() == 220)) || ((rectangle.getX() == 490) && (rectangle.getY() == 115))) {
            rectangle.translate(0, 105);
            pos.setY(rectangle.getY());
            pos.setX(rectangle.getX());
        }
    }


    public void moveUp() {

        int aux = rectangle.getY();
        if (((rectangle.getX() == 490) && (rectangle.getY() == 640)) || ((rectangle.getX() == 40) && (rectangle.getY() == 535)) || ((rectangle.getX() == 490) && (rectangle.getY() == 430)) || ((rectangle.getX() == 40) && (rectangle.getY() == 325)) || ((rectangle.getX() == 490) && (rectangle.getY() == 220))) {
            rectangle.translate(0, -105);
            pos.setY(rectangle.getY());
            pos.setX(rectangle.getX());
        }
    }

    public void jump() {

        if (rectangle.getY() == 640 || rectangle.getY() == 535 || rectangle.getY() == 430 || rectangle.getY() == 325 || rectangle.getY() == 220 || rectangle.getY() == 115) {
            rectangle.translate(0, -jumpSize);
            pos.setY(rectangle.getY());
            pos.setX(rectangle.getX());
            jumpCounter = 1;
        }
    }

    public void afterJump() {
        if (rectangle.getY() == 640-jumpSize || rectangle.getY() == 535-jumpSize || rectangle.getY() == 430-jumpSize || rectangle.getY() == 325-jumpSize || rectangle.getY() == 220-jumpSize || rectangle.getY()==115-jumpSize) {
            rectangle.translate(0, jumpSize);
            pos.setY(rectangle.getY());
            pos.setX(rectangle.getX());
            jumpCounter = 0;
        }
    }

    public void die () throws InterruptedException {

        lives--;
        if ( lives == 2){
            System.out.println("VIDA 2");
            life3.delete();
        }
        if (lives == 1){

            System.out.println("VIDA 1");
            life2.delete();
        }
        if (lives == 0){
            life1.delete();
            playerDead = true;
        }

        if(!musicBox.isMute()) {
            musicBox.deadMusic();
        }
        TimeUnit.MILLISECONDS.sleep(500);

        int initialCol = pos.getX();
        int initialRow = pos.getY();

        pos.setX(posXInit);
        pos.setY(posYInit);

        int dx = (posXInit) - initialCol;
        int dy = (posYInit) - initialRow;

        this.rectangle.translate(dx,dy);
    }

    public void livesStarter(){
        life1 = new Picture(30,30,"resources/images/lifeO.png");
        life1.draw();
        life2 = new Picture(70,30,"resources/images/lifeO.png");
        life2.draw();
        life3 = new Picture(110,30,"resources/images/lifeO.png");
        life3.draw();
    }

    public boolean isPlayerDead() { return playerDead; }

    public void playerDisappear() { rectangle.delete(); }

}
