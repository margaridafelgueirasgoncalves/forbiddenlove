package org.academiadecodigo.team2.forbiddenlove.gameobject;

public enum GameObjectType {

    PLAYER("Player"),
    BRICK("Brick"),
    ENEMY("Enemy"),
    LADDER("Ladder"),
    PAULO("Paulo"),
    FABIO ("Fabio");

    private String description;

    GameObjectType (String description) { this.description = description; }

    public String getDescription() {
        return description;
    }

}
