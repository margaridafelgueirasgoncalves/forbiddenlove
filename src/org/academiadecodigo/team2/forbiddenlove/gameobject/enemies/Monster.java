package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.gameobject.GameObject;
import org.academiadecodigo.team2.forbiddenlove.gameobject.GameObjectType;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;
import org.academiadecodigo.team2.forbiddenlove.screen.Screen;

public class Monster extends GameObject {

    Picture fabio;
    private int posX;
    private int posY;
    private final int height = 45;
    private final int width = 30;
    private final int moveSize = 15;
    private final int brickWidth = 15;
    private Position pos;
    private Direction direction;



    public Monster(Position pos, Direction direction) {
        super(pos.getX(), pos.getY(), GameObjectType.FABIO);
        this.posX=pos.getX();
        this.posY=pos.getY();
        this.direction=direction;
        this.pos=pos;
        this.fabio= new Picture(posX, posY, "resources/images/fabio_gameSize.png");
        fabio.draw();
        move();
    }


    public void moveInDirection(Direction direction) {

        int initialCol = pos.getX();
        int initialRow = pos.getY();

        this.pos.moveInDirection(direction);

        int dx = pos.getX() - initialCol;
        int dy = pos.getY() - initialRow;

        this.fabio.translate(dx,dy);

    }


    public void move() {
        if (pos.getX() + Screen.PADDING * 1.5 >= Screen.WIDTH + 10 -15) {
            direction = Direction.LEFT;
        }
        if (pos.getX() <= Screen.PADDING * 2.5 +15) {
            direction = Direction.RIGHT;
        }
        if (direction == Direction.RIGHT) {
            this.moveInDirection(Direction.RIGHT);
            return;
        }

        this.moveInDirection(Direction.LEFT);
    }

    public int getPosY() {
        return pos.getY();
    }

    public int getPosX() {
        return pos.getX();
    }

    public Position getPos() {
        return pos;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void fabioDisappear() { fabio.delete(); }
}
