package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;

public class Barras extends Enemy {

    private int counter = 0;
    private int initialX;
    private int initialY;
    private String path;

    public Barras(Position pos, Direction direction, String path) {

        super(pos, direction);
        initialX = pos.getX();
        initialY = pos.getY();

        rectangle = new Picture(x,y,path);
        rectangle.draw();
        width = rectangle.getWidth();
        height = rectangle.getHeight();
    }

    public void move() {
            counter++;
            if (counter > 15) {
                rectangle.delete();
                this.pos.setY(-100);
                this.pos.setX(-100);
            }
            if (counter > 40) {
                this.pos.setY(initialY);
                this.pos.setX(initialX);
                rectangle.draw();
                counter = 0;
            }
    }
}
