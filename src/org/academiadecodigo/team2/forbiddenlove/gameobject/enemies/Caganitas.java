package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;
import org.academiadecodigo.team2.forbiddenlove.screen.Screen;

public class Caganitas extends Enemy {

    public Caganitas (Position pos, Direction initialDirection) {
        super(pos, initialDirection);

        rectangle = new Picture(x,y,"resources/images/fabio1.png");
        rectangle.draw();
        width = rectangle.getWidth();
        height = rectangle.getHeight();
    }

    public void move() {

        if (pos.getX() + Screen.PADDING * 1.5 >= Screen.WIDTH + 10) {
            direction = Direction.LEFT;
            rectangle.load("resources/images/fabio1.png");
        }
        if (pos.getX() <= Screen.PADDING * 2.5) {
            direction = Direction.RIGHT;
            rectangle.load("resources/images/fabio2.png");
        }
        if (direction == Direction.RIGHT) {
            this.moveInDirection(Direction.RIGHT);
            return;
        }
        this.moveInDirection(Direction.LEFT);
    }

}
