package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;

public class CimaBaixo extends Enemy {

    public CimaBaixo (Position pos, Direction initialDirection) {
        super(pos, initialDirection);

        rectangle = new Picture(x,y,"resources/images/moustache.png");
        rectangle.draw();
        width = rectangle.getWidth();
        height = rectangle.getHeight();
    }


    public void move() {
        if (pos.getY() <= 385) {
            direction = Direction.DOWN;
        }
        if (pos.getY() >= 465) {
            direction = Direction.UP;
        }
        if (direction == Direction.UP) {
            this.moveInDirection(Direction.UP);
            return;
        }
        this.moveInDirection(Direction.DOWN);
    }
}
