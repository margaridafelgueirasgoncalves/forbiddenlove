package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;
import org.academiadecodigo.team2.forbiddenlove.screen.Screen;

public class Balas extends Enemy {

    public Balas (Position pos, Direction direction) {
        super(pos, direction);
        rectangle = new Picture(x,y,"resources/images/dildo.png");
        rectangle.draw();
        width = rectangle.getWidth();
        height = rectangle.getHeight();
    }

    public void move() {

        this.moveInDirection(Direction.RIGHT);
        if (pos.getX() +10 >= Screen.WIDTH) {
            int initialCol = pos.getX();
            int initialRow = pos.getY();

            this.pos.setX(80);

            int dx = pos.getX() - initialCol;
            int dy = pos.getY() - initialRow;

            this.rectangle.translate(dx,dy);
        }

    }
}
