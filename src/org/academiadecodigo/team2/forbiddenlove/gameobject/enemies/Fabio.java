package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;

public class Fabio extends Enemy {

    public Fabio (Position pos, Direction initialDirection) {
        super(pos,initialDirection);
    }

    public void move() {
    }
}
