package org.academiadecodigo.team2.forbiddenlove.gameobject.enemies;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.gameobject.GameObject;
import org.academiadecodigo.team2.forbiddenlove.gameobject.GameObjectType;
import org.academiadecodigo.team2.forbiddenlove.movement.Direction;
import org.academiadecodigo.team2.forbiddenlove.movement.Position;

public abstract class Enemy extends GameObject {

    protected Direction direction;
    protected Picture rectangle;
    protected Position pos;
    protected int width;
    protected int height;

    public Enemy (Position pos, Direction direction) {
        super(pos.getX(), pos.getY(), GameObjectType.ENEMY);
        this.direction = direction;
        this.pos = pos;

    }

    public Position getPos() { return pos; }

    public int getWidth() { return width;}

    public int getHeight() { return height;}

    public void moveInDirection(Direction direction) {

        int initialCol = pos.getX();
        int initialRow = pos.getY();

        this.pos.moveInDirection(direction);

        int dx = pos.getX() - initialCol;
        int dy = pos.getY() - initialRow;

        this.rectangle.translate(dx,dy);

    }

    public abstract void move();

    public void disappear() { rectangle.delete(); }
}
