package org.academiadecodigo.team2.forbiddenlove.gameobject.platforms;

public class Bricks extends Platforms {

    //Props
    private int x;
    private int y;

    //Constr
    public Bricks(int startingX , int startingY, String path){
        super(startingX, startingY, path);
        this.x = startingX;
        this.y = startingY;
    }
}
