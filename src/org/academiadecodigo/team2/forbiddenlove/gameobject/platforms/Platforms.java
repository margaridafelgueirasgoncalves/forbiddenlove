package org.academiadecodigo.team2.forbiddenlove.gameobject.platforms;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.team2.forbiddenlove.screen.Screen;

public abstract class Platforms {

    //Prop
    private Picture tiles;
    private int x;
    private int y;
    private int pictureSize;
    private String path;
    private Picture[] variousPlatforms;

    //Constr
    public Platforms(int x, int y, String path) {
        this.path = path;
        this.x = x + Screen.PADDING;
        this.y = y + Screen.PADDING;
        tiles = new Picture(this.x, this.y, path);
        pictureSize = (int) tiles.getWidth();
        tiles.draw();

    }
}
