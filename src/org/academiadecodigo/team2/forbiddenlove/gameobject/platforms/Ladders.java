package org.academiadecodigo.team2.forbiddenlove.gameobject.platforms;

public class Ladders extends Platforms {

    //Props
    private int x;
    private int y;

    public Ladders( int startingX, int startingY){
        super(startingX,startingY, "resources/images/stairs-30x105.png");
        this.x = startingX;
        this.y = startingY;
    }
}
