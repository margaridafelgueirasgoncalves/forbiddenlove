package org.academiadecodigo.team2.forbiddenlove.gameobject;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Princess {

    Picture paulo;
    private int posX = 25;
    private int posY = 115;

    public Princess() {
        this.paulo= new Picture(posX, posY, "resources/images/pauloJogoSize_quadradinho.png");
        paulo.draw();
    }

    public void pauloDisappear () { paulo.delete(); }

}
