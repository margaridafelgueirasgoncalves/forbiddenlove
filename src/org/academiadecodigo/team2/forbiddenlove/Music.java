package org.academiadecodigo.team2.forbiddenlove;

import org.academiadecodigo.bootcamp.Sound;


public class Music {

    private Sound musicMain = new Sound("/resources/sound/sandstorm.wav");
    private Sound deadSound = new Sound("/resources/sound/gameover.wav");
    private Sound introMusic = new Sound("/resources/sound/BadRomance.wav");
    private Sound gameOverMusic = new Sound("/resources/sound/gameoverfinalmusic.wav");
    private Sound winMusic = new Sound("/resources/sound/WinMusic.wav");
    private boolean mute;
    private boolean intro;
    private boolean dead;
    private boolean main;
    private boolean gameOver;
    private boolean win;


    public boolean isMute() { return mute; }
    public void setIntro(boolean intro) { this.intro = intro; }
    public void setMain(boolean main) { this.main = main; }
    public void setGameOver(boolean gameOver) { this.gameOver = gameOver; }
    public void setWin(boolean win) { this.win = win; }

    public void playWin() {
        musicMain.stop();
        main = false;
        win = true;
        winMusic.play(win);
    }

    public void startMusicMain() {
        if(intro) {
            introMusic.close();
            intro = false;
        }
        if(gameOver){
            gameOverMusic.stop();
            gameOver = false;
        }
        if(win) {
            winMusic.stop();
            win = false;
        }
        main = true;
        dead = true;
        musicMain.play(main);
    }

    public void deadMusic() {
        deadSound.play(dead);
    }

    public void introMusicPlay() {
        intro = true;
        introMusic.play(intro);
    }

    public void playGameOverMusic() {
        musicMain.stop();
        main = false;
        gameOver = true;
        gameOverMusic.play(gameOver);
    }

    public void mute() {
        if (!mute) {
            musicMain.stop();
            introMusic.stop();
            gameOverMusic.stop();
            winMusic.stop();
            dead = false;
            mute = true;

        } else {
            if(main) {
                musicMain.play(false);
            }
            if(intro) {
                introMusic.play(false);
            }
            if(gameOver) {
                gameOverMusic.play(false);
            }
            if(win) {
                winMusic.play(false);
            }
            dead = true;
            mute = false;
        }
    }
}
